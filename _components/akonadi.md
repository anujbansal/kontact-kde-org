---
layout: component
name: Akonadi
shortDescription:
description:
screenshot: /assets/img/akonadi.png
css-include: /css/component.css
order: 70
---

The Akonadi framework, named after the oracle goddess of justice in Ghana
is responsible for providing applications with a centralized database to
store, index and retrieve the user's personal information. This includes
the user's emails, contacts, calendars, events, journals, alarms, notes,
etc.

## Features
* Common PIM data cache
* Type agnostic design
* Extensibility
* Generic offline access, change recording and replay
* Generic conflict detection and resolution
* Resources are groupable by profile
* Items composed of independently retrievable multiple parts
* Zero-copy retrieval possible
* Concurrent access allows background activity independent of UI client
* Syncing mail, calendar, addressbooks to remote servers
* Permits semantic desktop infrastructure to access PIM data
* Archiving
* Indexing
* Out-of-process search
* Multi-process design
* Crash isolation
* Large items can't block whole system
* Linkage by IPC allows proprietary components
